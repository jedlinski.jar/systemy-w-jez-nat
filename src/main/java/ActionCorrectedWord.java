package main.java;

public class ActionCorrectedWord {

	private String originalWord;
	private String correctedWord;

	public ActionCorrectedWord(String originalWord, String correctedWord) {
		this.originalWord = originalWord;
		this.correctedWord = correctedWord;
	}

	public String getOriginalWord() {
		return originalWord;
	}

	public String getCorrectedWord() {
		return correctedWord;
	}
}
