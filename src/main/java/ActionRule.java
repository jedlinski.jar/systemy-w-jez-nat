package main.java;

/**
 * Dodatkowe operacje na czasownikach
 */
public enum ActionRule {

	UCZY("uczy", "uczyć"), //uczyć, kluczyć (ale buczeć, mruczeć :( )
	IAC("iąc", "ić"),
	MUJA("mują", "mować"),
	ERA("era", "erać"),
	JAL("jął", "jąć"),
	JAL2("jęła", "jąć"),
	ALA("ała", "ać"),
	ALA2("ał", "ać");


	private String suffix;
	private String replacement;

	ActionRule(String suffix, String replacement) {
		this.suffix = suffix;
		this.replacement = replacement;
	}

	boolean suffixMatches(String word) {

		String lowerCaseWord = word.toLowerCase();

		return lowerCaseWord.length() > suffix.length() && word.endsWith(suffix);
	}

	String removeSufix(String word) {

		String lowerCaseWord = word.toLowerCase();

		return lowerCaseWord.replaceFirst(suffix + "$", replacement);
	}
}
