package main.java;

import java.util.Arrays;

public class ActionsProcessor {

	ActionCorrectedWord processWord(StemmedWord stemmedWord) {

		String result = removeSuffix(stemmedWord.getAfterStemming());

		return new ActionCorrectedWord(stemmedWord.getOriginalWord(), result);
	}

	private String removeSuffix(String originalWord) {

		return Arrays.stream(ActionRule.values())
				.filter(stemmingSuffix -> stemmingSuffix.suffixMatches(originalWord))
				.findFirst()
				.map(stemmingSuffix -> stemmingSuffix.removeSufix(originalWord))
				.orElse(originalWord);
	}
}
