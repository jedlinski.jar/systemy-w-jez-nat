package main.java;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.commons.lang3.StringUtils;
import pl.sgjp.morfeusz.Morfeusz;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static javafx.collections.FXCollections.observableArrayList;

public class Controller {

	@FXML
	public TableView<ProcessedWord> table;

	private ObservableList<ProcessedWord> data = observableArrayList();
	private Stemmer stemmer = new Stemmer();
	private Depluralizer depluralizer = new Depluralizer();
	private ActionsProcessor actionsProcessor = new ActionsProcessor();

	private ObservableList<ProcessedWord> wordsInTable = observableArrayList(data);

	private Morfeusz morfeusz = Morfeusz.createInstance();

	@FXML
	private void initialize() throws IOException, URISyntaxException {

		String path = getClass().getResource("/corpus.txt").toURI().getPath();

		Files.lines(Paths.get(path)).forEach(this::processLine);

		configureColumns();

		table.setRowFactory(tv -> new TableRow<ProcessedWord>() {

			@Override
			protected void updateItem(ProcessedWord item, boolean empty) {
				super.updateItem(item, empty);

				if (empty) {
					return;
				}

				if (!item.isCorrect(morfeusz)) {
					setStyle("-fx-background-color: #FF0000");
				} else {
					setStyle("-fx-background-color: #00FF00");
				}
			}
		});
		table.setItems(data);
	}

	private void configureColumns() {

		TableColumn<ProcessedWord, String> col1 = new TableColumn<>("Słowo");
		col1.setCellValueFactory(new PropertyValueFactory<>("resultWord"));
		TableColumn<ProcessedWord, String> col2 = new TableColumn<>("Słowa źródłowe");
		col2.setCellValueFactory(new PropertyValueFactory<>("sourceWords"));
		TableColumn<ProcessedWord, String> col3 = new TableColumn<>("Liczba powtórzeń");
		col3.setCellValueFactory(new PropertyValueFactory<>("occurences"));

		TableColumn[] columns = {col1, col2, col3};
		List<TableColumn> tableColumns = asList(columns);
		tableColumns.forEach(col -> col.prefWidthProperty().bind(table.widthProperty().divide(columns.length).subtract(1)));

		table.getColumns().addAll(columns);
	}

	private void processLine(String line) {

		if (line.contains("wikipedia")){
			processWikipediaSource(line);
		}else {
			processFile(line);
		}
	}

	private void processWikipediaSource(String line) {

		String wikipediaText = WikipediaTextExtractor.extractWikipediaTextFromUrl(line);

		processText(wikipediaText);
	}

	private void processFile(String file) {

		try {

			String text = new String(Files.readAllBytes(Paths.get(file)));

			//processText(text);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void processText(String text) {

		text = text.replace("\n", " "); //usuwamy znaki nowej linii
		final List<String> words = stream(text.split(" "))
				.filter(StringUtils::isAlpha) //odrzucamy słowa zawierające inne znaki niż litery
				.flatMap(word -> stream(word.split("[.,?!]"))) //dzielimy slowa po znakach specjalnych
				.collect(toList());

		List<ProcessedWord> processedWords = words.stream()
				.map(depluralizer::depluralize)
				.map(stemmer::processWord)
				.map(actionsProcessor::processWord)
				.map(ProcessedWord::new)
				.collect(toList());

		processedWords.forEach(processedWord -> {
			List<ProcessedWord> list = processedWords.stream()
					.filter(word -> word.equals(processedWord))
					.collect(toList());

			processedWord.setOccurences(list.size());

			list.stream()
					.map(ProcessedWord::getAllSourceWords)
					.forEach(processedWord::addSourceWords);
		});

		List<ProcessedWord> distinctWords = processedWords.stream()
				.distinct()
				.collect(toList());

		distinctWords.forEach(word -> words.removeAll(word.getAllSourceWords()));

		data.clear();
		table.getItems().clear();
		data.addAll(distinctWords);
	}

	@FXML
	public void check() {

		long count = data.stream()
				.filter(word -> word.isCorrect(morfeusz))
				.count();

		System.out.println("Poprawnych: " + count + "/" + data.size());
	}
}
