package main.java;



public enum DepluralizationRule {

	//zamieniamy 1. na 2.
	D_FUN_1("czki", "czka"),
	D_FUN_2("jęć", "jęcia"),
	D_FUN_3("tów", "t"),
	D_FUN_4("nicy", "nik"),
	D_FUN_5("czą", "czy"),
	D_FUN_6("nych", "nego"),
	D_FUN_7("dzi", "dzić"),
	D_FUN_8("cych", "cy"),
	D_FUN_9("na", "ny") {
		@Override
		boolean isPlural(String word) {
			return super.isPlural(word) && word.length() > 3;
		}
	},
	//D_FUN_10("nie", "ni"),
	D_FUN_11("nia", "nie"),
	D_FUN_14("niu", "nie"),
	D_FUN_12("yły", "ył"),
	TKICH1() {
		@Override
		boolean isPlural(String word) {
			return word.matches(SAMOGLOSKA + SPÓŁGŁOSKA + "kich$"); //słowa typu "krótkich" "leciutkich" ale nie "wszystkich"...
		}

		@Override
		String depluralize(String originalWord) {
			return originalWord.replaceFirst("tkich$", "tki");
		}
	},
	TKICH2() {
		@Override
		boolean isPlural(String word) {
			return word.matches(SPÓŁGŁOSKA + SPÓŁGŁOSKA + "kich$"); //odrzucone w przypadku wyzej
		}

		@Override
		String depluralize(String originalWord) {
			return originalWord.replaceFirst("tkich$", "cy");
		}
	},
	NIAMI("niami", "nie"),
	//KAMI("k", "kami"),
	EJA("eją", "eć"),
	ACH("kach", "ka"),
	TYCH("tych", "ty"),
	STWO("stw", "stwo"),
	SKICH("skich", "ski");


	private static final String SAMOGLOSKA = "[uóęaeiyo]";
	private static final String SPÓŁGŁOSKA = "[^uóęaeiyo]";

	private String suffix;
	private String replacement;

	DepluralizationRule() {
	}

	DepluralizationRule(String suffix, String replacement) {
		this.suffix = suffix;
		this.replacement = replacement;
	}

	boolean isPlural(String word) {

		String lowerCaseWord = word.toLowerCase();

		return lowerCaseWord.endsWith(suffix) && word.length() > suffix.length();
	}

	String depluralize(String originalWord) {

		String lowerCaseWord = originalWord.toLowerCase();

		return lowerCaseWord.replaceFirst(suffix + "$", replacement);
	}
}
