package main.java;

public class DepluralizedWord {

	private String originalWord;
	private String afterDepluralization;

	public DepluralizedWord(String originalWord, String afterDepluralization) {
		this.originalWord = originalWord;
		this.afterDepluralization = afterDepluralization;
	}

	public String getOriginalWord() {
		return originalWord;
	}

	public String getAfterDepluralization() {
		return afterDepluralization;
	}
}
