package main.java;

import java.util.Arrays;

public class Depluralizer {


	DepluralizedWord depluralize(String originalWord) {

		String newWord = Arrays.stream(DepluralizationRule.values())
				.filter(rule -> rule.isPlural(originalWord))
				.findFirst()
				.map(rule -> rule.depluralize(originalWord))
				.orElse(originalWord);

		return new DepluralizedWord(originalWord, newWord);
	}
}
