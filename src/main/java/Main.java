package main.java;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import pl.sgjp.morfeusz.Morfeusz;
import pl.sgjp.morfeusz.MorphInterpretation;
import pl.sgjp.morfeusz.app.MorfeuszUtils;

import java.util.List;

public class Main extends Application {

    @FXML
    private TableView table;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        primaryStage.setTitle("Słownik");
        Scene scene = new Scene(root, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();


		Morfeusz instance = Morfeusz.createInstance();
		List<MorphInterpretation> result = instance.analyseAsList("pucyb");
		//int tag = instance.getIdResolver().getTagId("subst:sg:nom:m");
		String interpretationString = MorfeuszUtils.getInterpretationString(result.get(0), instance);
		System.out.println(interpretationString);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
