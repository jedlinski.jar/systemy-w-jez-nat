package main.java;

import pl.sgjp.morfeusz.Morfeusz;
import pl.sgjp.morfeusz.app.MorfeuszUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.joining;

public class ProcessedWord {

	private String resultWord;
	private List<String> sourceWords;
	private int occurences;
	private Boolean isCorrect;

	public ProcessedWord(String resultWord, String sourceWord) {
		this.resultWord = resultWord;
		sourceWords = new ArrayList<>();
		sourceWords.add(sourceWord);
	}

	public ProcessedWord(ActionCorrectedWord actionCorrectedWord) {

		resultWord = actionCorrectedWord.getCorrectedWord();
		sourceWords = new ArrayList<>();
		sourceWords.add(actionCorrectedWord.getOriginalWord());
	}

	public String getResultWord() {
		return resultWord;
	}

	public String getSourceWords() {
		return sourceWords.stream().collect(joining(", "));
	}

	public List<String> getAllSourceWords() {
		return sourceWords;
	}

	public int getOccurences() {
		return occurences;
	}

	public void increaseOccurences() {
		occurences += 1;
	}

	public void addSourceWords(List<String> words) {

		words.forEach(word -> {
			if (!sourceWords.contains(word)) {
				sourceWords.add(word);
			}
		});
	}

	public boolean isCorrect(Morfeusz morfeusz) {

		return morfeusz.analyseAsList(resultWord).stream()
				.map(word -> MorfeuszUtils.getInterpretationString(word, morfeusz))
				.peek(System.out::println)
				.anyMatch(label -> label.contains(":nom") ||
						label.contains("prep:") ||
						label.contains("inf:") ||
						label.contains("qub") ||
						label.contains("comp") ||
						label.contains("conj") ||
						label.contains("adv"));

		/*if (isCorrect != null) {
			return isCorrect;
		}

		try {
			String encodedWord = URLEncoder.encode(getResultWord().toLowerCase(), "UTF-8");

			URL url = new URL("https://glosbe.com/gapi/translate?from=pol&dest=pol&format=xml&phrase=" + encodedWord);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			StringBuilder stringBuilder = new StringBuilder();
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				stringBuilder.append(inputLine);
			}
			in.close();

			String xml = stringBuilder.toString();

			isCorrect = xml.contains("<string>tuc</string>");

			return isCorrect;
		} catch (Exception e) {

			e.printStackTrace();

			return false;
		}*/
	}

	public void setOccurences(int occurences) {
		this.occurences = occurences;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ProcessedWord word = (ProcessedWord) o;
		return word.getResultWord().equalsIgnoreCase(getResultWord());
	}

	@Override
	public int hashCode() {

		return Objects.hash(resultWord.toLowerCase());
	}
}
