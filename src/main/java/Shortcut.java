package main.java;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.stream;

public enum Shortcut {

	MGR("mgr.", "mgr"),
	NP("np.", "np"),
	TJ("tj.");

	private List<String> shortcutWords;

	Shortcut(String ... words) {
		this.shortcutWords = Arrays.asList(words);
	}

	public String getFirstWord() {
		return this.shortcutWords.get(0);
	}

	public static Optional<Shortcut> findByWord(String word) {

		return stream(values())
				.filter(value -> value.shortcutWords.contains(word))
				.findFirst();
	}
}
