package main.java;

public class StemmedWord {

	private String originalWord;
	private String afterStemming;

	public StemmedWord(String originalWord, String afterStemming) {
		this.originalWord = originalWord;
		this.afterStemming = afterStemming;
	}

	public String getOriginalWord() {
		return originalWord;
	}

	public String getAfterStemming() {
		return afterStemming;
	}
}
