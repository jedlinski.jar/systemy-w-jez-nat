package main.java;

import java.util.Arrays;

public class Stemmer {

	StemmedWord processWord(DepluralizedWord depluralizedWord) {


		String withoutPrefix = removePrefix(depluralizedWord.getAfterDepluralization());
		String result = removeSuffix(withoutPrefix);

		return new StemmedWord(depluralizedWord.getOriginalWord(), result);
	}

	private String removePrefix(String originalWord) {

		return Arrays.stream(StemmingPrefix.values())
					.filter(stemmingPrefix -> stemmingPrefix.prefixMatches(originalWord)) //usuwamy tylko pierwszy znaleziony prefix
					.findFirst()
					.map(stemmingPrefix -> stemmingPrefix.removePrefix(originalWord))
					.orElse(originalWord);
	}

	private String removeSuffix(String originalWord) {

		return Arrays.stream(StemmingSuffix.values())
				.filter(stemmingSuffix -> stemmingSuffix.suffixMatches(originalWord))
				.findFirst()
				.map(stemmingSuffix -> stemmingSuffix.removeSufix(originalWord))
				.orElse(originalWord);
	}
}
