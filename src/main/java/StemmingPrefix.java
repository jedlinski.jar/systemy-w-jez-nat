package main.java;

import java.util.function.Predicate;

public enum StemmingPrefix {

	DO("do", word -> word.endsWith("yć")),
	PRZY("przy", word -> word.endsWith("yć")),
	NIE("nie");


	private String prefix;
	private Predicate<String> condition;

	StemmingPrefix(String prefix) {
		this.prefix = prefix;
		condition = word -> true;
	}

	StemmingPrefix(String prefix, Predicate<String> condition) {
		this.prefix = prefix;
		this.condition = condition;
	}

	public String getPrefix() {
		return prefix;
	}

	boolean prefixMatches(String word) {

		String lowerCaseWord = word.toLowerCase();

		return lowerCaseWord.length() > prefix.length() && lowerCaseWord.startsWith(prefix) && condition.test(word);
	}

	String removePrefix(String word) {

		String lowerCaseWord = word.toLowerCase();

		return lowerCaseWord.replaceFirst("^" + prefix, ""); //jesli jest prefix to go usuwamy
	}
}
