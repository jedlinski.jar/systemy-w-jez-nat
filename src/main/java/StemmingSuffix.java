package main.java;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum StemmingSuffix {

	WANIA("wanie", "wania"), //lemm
	ESIE("es", "esie"), //lemm
	JACE("jący", "jące"), //lemm
	ZY("żyć", "ży"), //
	WIANIE("wianie", "wiania"),
	OSCI("ość", "ości"),
	OCEJ("ący", "ącej"),
	NYCH("ny", "nych", "nej", "nym"),
	JECIA("jęcie", "jęcia"),
	MOCA("moc", "mocą"),
	NIKA("nik", "nika"),
	KIEM("yk", "ykiem", "yka", "yku", "ykiem", "ykach", "yki", "yków"),
	KOWYM("owy", "owym"),
	ANA("any", "ana"),
	MU("m", "mu"),
	AJA("ać", "ają"),
	ONA("ony", "oną"),
	NY("ny", "nego", "nym"),
	ENIA("enie", "enia"),
	AKU("ak", "aku"),
	LEGO("ły", "łego"),
	ORU("ór", "oru"),
	YL("yć", "ył", "yła", "yły"),
	LY("ć", "ły"),
	JA("cja", "cji", "cją", "cję", "cjo"),
	BE("ba", "bę", "by", "bie", "bach") {
		@Override
		boolean suffixMatches(String word) {
			return super.suffixMatches(word) && word.length() > 3;
		}
	},
	YCH("owy", "owych", "owa", "owe", "owego", "owej", "owa"),
	DZKA("dzki", "dzką", "dzka"),
	ACZA("acz", "acza", "aczają"),
	EGU("eg", "egu", "egach", "egi", "egiem"),
	KIM("ki", "kim", "kiego", "kiej"),
	IOCIE("iot", "iocie"),
	TKY("tek", "tku", "tkach", "tkiem"),
	NAJE("nawać", "naje"),
	ANIE("anie", "aniu"),
	RZE("r", "rze", "rach", "rem"),
	DKU("dek", "dku", "dkiem", "dkach"),
	DEM("d", "dem", "dach", "dzie"),
	OTE("ota", "otę", "ocie", "otach", "otami"),
	TEGO("ty", "tego"),
	OPIE("op", "opie"),
	SPIE("spa", "spie"),
	NKIEM("nek", "nkiem"),
	II("ia", "ii"),
	KIEJ("ka", "kiej"),
	EGO("cy", "cego", "cej"),
	WIATA("wiat", "wiata"),
	IATA("iat", "iata"),
	DZIE("", "dzie"),
	UDU("ud", "udu", "udzie"),
	YZNA("yzna", "yznę", "yznie"),
	RMA("rma", "rmie", "rmą", "rmę"),
	CY("cy", "ców", "cami"),
	RAZ("raz", "razu", "razem", "razie"),
	GI("gi", "giej", "giego", "gim"),

	MI("", "mi");

	private String sufix;
	private List<String> conditionSuffixes;

	StemmingSuffix(String sufix, String ... conditionSufixes) {
		this.sufix = sufix;
		conditionSuffixes = Arrays.asList(conditionSufixes);
	}

	public String getSufix() {
		return sufix;
	}

	boolean suffixMatches(String word) {

		String lowerCaseWord = word.toLowerCase();

		return lowerCaseWord.length() > sufix.length() && anyConditionalSuffixMatches(word);
	}

	String removeSufix(String word) {

		String lowerCaseWord = word.toLowerCase();

		Optional<String> optionalCondition = findFirstMatchingCondition(word);

		return optionalCondition.map(conditionSuffix -> lowerCaseWord.replaceFirst(conditionSuffix + "$", sufix)).orElse(word);
	}

	private Optional<String> findFirstMatchingCondition(String word) {

		return conditionSuffixes.stream()
				.filter(suffix -> word.toLowerCase().endsWith(suffix))
				.findFirst();
	}

	private boolean anyConditionalSuffixMatches(String word) {

		return findFirstMatchingCondition(word).isPresent();
	}
}
