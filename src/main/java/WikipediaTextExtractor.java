package main.java;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

class WikipediaTextExtractor {

	static String extractWikipediaTextFromUrl(String line) {

		try {
			URL url = new URL(line);
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

			StringBuilder stringBuilder = new StringBuilder();
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				stringBuilder.append(inputLine);
			}

			String html = stringBuilder.toString();

			Document doc = Jsoup.parse(html);
			Elements paragraphs = doc.select("p");

			return paragraphs.text();

		} catch (Exception e) {
			e.printStackTrace();
		}

		throw new IllegalStateException("Nie udało się odczytać tekstu z adresu: " + line);
	}
}
