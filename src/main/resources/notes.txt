subst - rzeczownik
sg - liczba pojedyncza
mianownik nom
dopełniacz gen
celownik dat
biernik acc
narzędnik inst
miejscownik loc
wołacz voc

mX - rodzaj męski
inf - bezokolicznik
perf - dokonany
adv - przysłówek odprzymiotnikowy i/lub stopniowalny (dotychczas)
qub - kublik (np. się)